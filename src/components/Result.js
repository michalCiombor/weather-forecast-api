import React from "react";
import "./Result.css";

const Result = props => {
  const {
    date,
    city,
    sunrise,
    sunset,
    temp,
    pressure,
    wind,
    err
  } = props.weather;

  let content = null;

  if (!err && city) {
    const sunriseTime = new Date(sunrise * 1000).toLocaleTimeString();
    const sunsetTime = new Date(sunset * 1000).toLocaleTimeString();

    content = (
      <>
        <h3>
          Wyniki wyszukiwania dla <em>{city}</em>
        </h3>
        <h4>
          Dane dla dnia i godziny:<b>{date}</b>{" "}
        </h4>
        <h4>
          Aktualna temperatura:<b>{temp} &#176;C</b>{" "}
        </h4>
        <h4>
          Wschód słońca dzisiaj o<b>{sunriseTime}</b>{" "}
        </h4>
        <h4>
          Zachód słońca dzisiaj o<b>{sunsetTime}</b>{" "}
        </h4>
        <h4>
          Aktualna siła wiatru <b>{wind} </b>m/s
        </h4>
        <h4>
          Aktualna ciśnienie<b> {pressure}</b> hPa
        </h4>
        <hr />
      </>
    );
  }

  return (
    <div className="result">{err ? `Nie mamy w bazie ${city}` : content}</div>
  );
};

export default Result;
